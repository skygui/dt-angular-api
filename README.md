# This is a readme
---
## How to Run
```sh
mkdir -p dt-angular
cd dt-angular

### Choose one ###
#git clone https://gitlab.com/phinlander/dt-angular-api.git
#git clone git@gitlab.com:phinlander/dt-angular-api.git

./dt-angular-api/docker/init.sh dt-angular-api <port>

mysql -u <user> -p < scripts/init.sql

### TEMPORARY ###
# Grab any updates that were made to make this example work while they get merged in
cd dt-angular-api
cp -r updates/* vendor/
cd ../
./docker/composer dt-angular-api install
```


### Apache and Local
Make sure and copy the dt-angular-api.conf from the apache folder into the deepthought root apache dir. Also copy over the json files from local into deepthought's local. They should be the same as dt-angular-web's.

### Scripts
There is an init.sql which can be run to generate a fully working sample db that works with this code as-is.

### WARNING
There may be modified DTProvider* code in here from development that is not yet live. If everything breaks, it's because these changes weren't pulled in yet.