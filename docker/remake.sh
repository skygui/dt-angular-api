#!/bin/sh

docker stop $1 && \
docker rm -v $1 && \
./docker/run.sh $1 $2