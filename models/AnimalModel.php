<?php
  class BaseAnimal extends DTModel {
    protected static $storage_table = "animal";

    public $name;
  }

  class Animal extends BaseAnimal {
    protected static $has_many_manifest = array(
      "planets" => array("AnimalHasPlanet", "AnimalPlanet")
    );
    public $planets;
  }

  class PlanetAnimal extends BaseAnimal { }