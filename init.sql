create database if not exists dt_angular_api;
use dt_angular_api;

drop table if exists consumers;
drop table if exists tokens;
drop table if exists planet_has_animal;
drop table if exists animal;
drop table if exists planet;

create table animal (
	id int primary key auto_increment,
    name text
) engine = innodb;

create table planet (
	id int primary key auto_increment,
    name text,
    position int
) engine = innodb;

create table planet_has_animal (
	planet_id int,
    animal_id int,
    primary key (planet_id, animal_id),
    foreign key (planet_id)
		references planet(id)
        on delete cascade,
	foreign key (animal_id)
		references animal(id)
        on delete cascade
) engine = innodb;

drop user if exists 'dt-angular-api'@'%';
create user 'dt-angular-api'@'%' identified by 'password';
grant all on dt_angular_api.* to 'dt-angular-api'@'%';

insert into animal (name) values ('lion'),('tiger'),('bear'),('dog'),('cat');
insert into planet (name) values ('earth'),('mars'),('venus'),('jupiter'),('pluto');
insert into planet_has_animal (planet_id, animal_id) values (1,1),(1,3),(2,5),(2,4),(3,1),(3,2),(3,3),(5,5);

CREATE TABLE tokens (
   id integer primary key auto_increment,
   type integer DEFAULT 0,
   status integer DEFAULT 0,
   token text,
   secret text,
   consumer_id integer,
   user_id integer,
   oauth_callback text
);

CREATE TABLE consumers (
   id integer primary key auto_increment,
   name text,
   consumer_key text,
   secret text,
   status integer DEFAULT 1,
   verifier text
);

INSERT INTO consumers (name, consumer_key, secret, verifier) values ("ccua-site","5eb63bbbe01eeed093cb22bb8f5acdc3","c0b0ef2d0f76f0133b83a9b82c1c7326","");